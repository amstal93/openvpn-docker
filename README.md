# บันทึก: OpenVPN ผ่าน docker (REAL CASE)

basic config for create private VPN on VPS

## กำหนดค่า IP และ ชื่อ Client
```
mkdir openvpn/vpn-data -p
cd ./openvpn
export vps_ip_address=222.222.222.222
export client_name=example_name
```

## 1 สร้าง config แบบอัตโนมัติ
```
docker run -v $(pwd)/vpn-data:/etc/openvpn \
--rm kylemanna/openvpn ovpn_genconfig -u udp://${vps_ip_address}
```

## 2 กำหนด `รหัส` และ `hostname/username` เพื่อสร้าง key
```
docker run -v $(pwd)/vpn-data:/etc/openvpn \
--rm -it kylemanna/openvpn ovpn_initpki
```

## 3 สั่งรันเซอร์วิซ OpenVPN
(recall again if stopped)
```
docker run -v $(pwd)/vpn-data:/etc/openvpn -d --name my-openvpn \
-p 1194:1194/udp --cap-add=NET_ADMIN --restart unless-stopped kylemanna/openvpn
```

## 4 สร้างใบรับรอง client
```
docker run -v $(pwd)/vpn-data:/etc/openvpn \
--rm -it kylemanna/openvpn easyrsa build-client-full ${client_name} nopass
```

## 5 ดึงไฟล์รับรอง (.ovpn) ออกมาเพื่อใช้งาน
```
docker run -v $(pwd)/vpn-data:/etc/openvpn \
--rm kylemanna/openvpn ovpn_getclient ${client_name} > ${client_name}.ovpn
```

## software download OpenVPN for Win10
```
URL: https://openvpn.net/client-connect-vpn-for-windows/
```

TKVR
